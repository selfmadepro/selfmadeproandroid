﻿using Firebase.Auth;
using SelfMadeProAndroid.Droid;
using SelfMadeProAndroid.Models;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(FirebaseAuthenticator))]
namespace SelfMadeProAndroid.Droid
{
    public class FirebaseAuthenticator : IFirebaseAuthenticator
    {
        public async Task<AuthResult> SignInWithFacebookTokenAsync(string accessToken)
        {
            // получение credentials по facebook токену
            var facebookCredentials = FacebookAuthProvider.GetCredential(accessToken);

            // авторизация в Firebase при помощи facebook credentials
            var authResult = await FirebaseAuth.Instance.SignInWithCredentialAsync(facebookCredentials);

            // получение Firebase токена для дальнейшего использования
            var getTokenResult = await authResult.User.GetIdTokenAsync(false);
            return new AuthResult()
            {
                Token = getTokenResult.Token,
                Uid = authResult.User.Uid
            };
        }

        public async Task<AuthResult> SignInWithEmailAndPasswordAsync(string email, string password)
        {
            // запрос входа польователя с адресом и паролем
            var authResult = await FirebaseAuth.Instance.SignInWithEmailAndPasswordAsync(email, password);

            // получение токена
            var getTokenResult = await authResult.User.GetIdTokenAsync(false);
            return new AuthResult()
            {
                Token = getTokenResult.Token,
                Uid = authResult.User.Uid,
                IsEmailVerified = authResult.User.IsEmailVerified
            };
        }

        public async Task<AuthResult> CreateUserWithEmailAndPasswordAsync(string email, string password)
        {
            // запрос создания польователя с адресом и паролем
            var authResult = await FirebaseAuth.Instance.CreateUserWithEmailAndPasswordAsync(email, password);

            await authResult.User.SendEmailVerificationAsync(ActionCodeSettings.NewBuilder().Build());
            // получение токена
            var getTokenResult = await authResult.User.GetIdTokenAsync(false);
            return new AuthResult()
            {
                Token = getTokenResult.Token,
                Uid = authResult.User.Uid,
                IsEmailVerified = authResult.User.IsEmailVerified
            };
        }

        public async Task SendPasswordResetEmailAsync(string email)
        {
            // отправка письма для сброса пароля на указанный адрес
            await FirebaseAuth.Instance.SendPasswordResetEmailAsync(email);
        }

        public async Task SendEmailVerificationAsync()
        {
            // отправка письма для подтверждения email
            await FirebaseAuth.Instance.CurrentUser.SendEmailVerificationAsync(ActionCodeSettings.NewBuilder().Build());
        }

        public async Task<bool> IsCurrentUserEmailVerified()
        {
            // обновление данных авторизированного пользователя
            await FirebaseAuth.Instance.CurrentUser.ReloadAsync();

            // получение ответа
            return FirebaseAuth.Instance.CurrentUser.IsEmailVerified;
        }
    }
}