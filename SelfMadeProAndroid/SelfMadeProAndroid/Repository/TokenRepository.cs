﻿using Xamarin.Essentials;

namespace SelfMadeProAndroid.Repository
{
    public static class TokenRepository
    {
        public static string GetFirebaseToken()
        {
            try
            {
                return SecureStorage.GetAsync("firebaseToken").Result;
            }
            catch
            {
                return null;
            }
        }

        public static void StoreFirebaseToken(string token)
        {
            try
            {
                SecureStorage.SetAsync("firebaseToken", token);
            }
            catch
            {
            }
        }
    }
}
