﻿using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using SelfMadeProAndroid.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace SelfMadeProAndroid.Repository
{
    public static class ChatRepository
    {
        public static async Task<List<Chat>> GetCurrentProfileChatsAsync()
        {
            try
            {
                var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
                using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
                {
                    var chats = await firebase.Child("Chats").OnceAsync<Chat>();
                    return chats
                        .Where(x => x.Object.Profiles.Any(p => p.Uid == ProfileRepository.CurrentProfile.Uid))
                        .Select(x => x.Object)
                        .ToList();
                }
            }
            catch
            {
                return new List<Chat>();
            }
        }
        public static async Task<Chat> GetChat(Guid id)
        {
            var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
            using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
            {
                return await firebase.Child("Chats").Child(id.ToString()).OnceSingleAsync<Chat>();
            }
        }

        public static ObservableCollection<ChatMessage> GetChatMessages(Guid id)
        {
            var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
            //using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
            {
                FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt);
                return firebase.Child("Chats").Child(id.ToString()).Child(nameof(Chat.Messages)).OrderBy(nameof(ChatMessage.CreatedOn))
                    .AsObservable<ChatMessage>().ObserveOn(Scheduler.Immediate).AsObservableCollection();
            }
        }

        public static async Task AddMessageToChat(Guid chatId, string message, int lastIndex)
        {
            var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
            using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
            {
                var newMessage = new ChatMessage()
                {
                    Sender = ProfileRepository.CurrentProfile.GetBaseClone(),
                    CreatedOn = DateTime.Now,
                    Text = message
                };
                await firebase.Child("Chats")
                              .Child(chatId.ToString())
                              .Child(nameof(Chat.Messages))
                              .Child(lastIndex.ToString())
                              .PutAsync(JsonConvert.SerializeObject(newMessage));
            }
        }

        public static async Task<Chat> GetOrCreateChatWithProfile(BaseProfile selectedProfile)
        {
            var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };

            var profiles = new[] { selectedProfile, ProfileRepository.CurrentProfile.GetBaseClone() };
            Chat chat;
            using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
            {
                var chats = await firebase.Child("Chats").OnceAsync<Chat>();

                // попытка получить личный чат двух профилей: текущего и выбранного
                chat = chats
                    .Where(x => profiles.Length == x.Object.Profiles.Count &&
                                profiles.All(p => x.Object.Profiles.Any(cp => cp.Uid == p.Uid)) &&
                                x.Object.Profiles.All(p => profiles.Any(cp => cp.Uid == p.Uid)))
                    .Select(x => x.Object).SingleOrDefault();
            }

            if (chat == null)
            {
                // в случае отсутствия такого чата - создаем
                chat = new Chat()
                {
                    Profiles = new List<BaseProfile>(profiles)
                };
                using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
                {
                    await firebase.Child("Chats").Child(chat.Id.ToString()).PutAsync(JsonConvert.SerializeObject(chat));
                }
            }

            return chat;
        }
    }
}
