﻿using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using SelfMadeProAndroid.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfMadeProAndroid.Repository
{
    public static class ProfileRepository
    {
        public static Profile CurrentProfile { get; set; }

        public static async Task SaveProfileAsync(Profile profile)
        {
            var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
            using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
            {
                await firebase.Child("Profiles").Child(profile.Uid).PutAsync(JsonConvert.SerializeObject(profile));
            }
        }

        public static async Task<Profile> GetProfileAsync(string uid)
        {
            try
            {
                var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
                using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
                {
                    return await firebase.Child("Profiles").Child(uid).OnceSingleAsync<Profile>();
                }
            }
            catch
            {
                return null;
            }
        }

        public static async Task<List<BaseProfile>> GetProfilesAsync()
        {
            try
            {
                var opt = new FirebaseOptions() { AuthTokenAsyncFactory = () => Task.FromResult(TokenRepository.GetFirebaseToken()) };
                using (FirebaseClient firebase = new FirebaseClient("https://selfmadeproandroid.firebaseio.com/", opt))
                {
                    var profiles = await firebase.Child("Profiles").OnceAsync<Profile>();
                    return new List<BaseProfile>(profiles.Select(x => x.Object.GetBaseClone()));
                }
            }
            catch
            {
                return new List<BaseProfile>();
            }
        }
    }
}
