﻿using SelfMadeProAndroid.Models;
using SelfMadeProAndroid.Repository;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SelfMadeProAndroid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatPage : ContentPage
    {
        public ChatPage(Chat chat)
        {
            InitializeComponent();
            BindingContext = this;

            RefreshChat(chat);
        }

        private async void RefreshChat(Chat chat)
        {
            Chat = await ChatRepository.GetChat(chat.Id);
            if (Chat.Messages.Count > 0)
            {
                SubscribeToMessages(Chat);
            }

            Title = Chat.Title;
        }

        public Chat Chat { get; set; }

        public ObservableCollection<ChatMessage> Messages { get; set; } = new ObservableCollection<ChatMessage>();

        private void ChatMessagesItemTapped(object sender, ItemTappedEventArgs e)
        {
            ChatMessagesListView.SelectedItem = null;
        }

        private void SubscribeToMessages(Chat chat)
        {
            Messages = ChatRepository.GetChatMessages(chat.Id);
            OnPropertyChanged(nameof(Messages));
            Device.StartTimer(TimeSpan.FromSeconds(0.1), () =>
            {
                ChatMessagesListView.ScrollTo(Messages.LastOrDefault(), ScrollToPosition.End, true);
                return true;
            });
        }

        private async void SendMessageClicked(object sender, System.EventArgs e)
        {
            var message = MessageEntry.Text;
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            var messageIndex = Messages.Count;
            await ChatRepository.AddMessageToChat(Chat.Id, message, messageIndex);
            
            MessageEntry.Text = null;
            MessageEntry.Unfocus();

            if (messageIndex == 0)
            {
                SubscribeToMessages(Chat);
            }
        }

        private void MessageEntryFocused(object sender, FocusEventArgs e)
        {
            Device.StartTimer(TimeSpan.FromSeconds(0.1), () =>
            {
                ChatMessagesListView.ScrollTo(Messages.LastOrDefault(), ScrollToPosition.End, true);
                return true;
            });
        }
    }
}