﻿using Newtonsoft.Json;
using Xamarin.Forms;

namespace SelfMadeProAndroid.Models
{
    public class BaseProfile
    {
        public string Uid { get; set; } // идентификатор пользователя в Firebase
        public string DisplayName { get; set; }
        public string PictureSource { get; set; }

        [JsonIgnore]
        public ImageSource ImageSource
        {
            get
            {
                if (string.IsNullOrEmpty(PictureSource))
                {
                    return new FileImageSource() { File = "profile.png" };
                }
                else
                {
                    return new UriImageSource() { Uri = new System.Uri(PictureSource) };
                }
            }
        }
    }
}
