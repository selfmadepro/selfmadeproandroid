﻿using System;

namespace SelfMadeProAndroid.Models
{
    public class ChatMessage
    {
        public string Text { get; set; }
        public BaseProfile Sender { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
