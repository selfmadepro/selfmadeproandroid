﻿using Newtonsoft.Json;

namespace SelfMadeProAndroid.Models
{
    public class FacebookProfile
    {
        [JsonProperty("id")]
        public string FacebookUserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("picture")]
        public FacebookProfilePicture Picture { get; set; }
    }
}
