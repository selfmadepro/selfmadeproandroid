﻿namespace SelfMadeProAndroid.Models
{
    public class Profile : BaseProfile
    {
        private Profile()
        { // приватный конструктор для того, чтобы избежать создания профиля напрямую, использовать только методы ниже
        }

        public string FacebookUserId { get; set; } // идентификатор пользователя в Facebook (null в случае регистрации через email)
        public string Email { get; set; } 
        public bool IsEmailVerified { get; set; }

        public BaseProfile GetBaseClone()
        {
            return new BaseProfile()
            {
                Uid = Uid,
                DisplayName = DisplayName,
                PictureSource = PictureSource
            };
        }

        public static Profile FromFacebookProfile(FacebookProfile facebookProfile)
        {
            return new Profile()
            {
                FacebookUserId = facebookProfile.FacebookUserId,
                Email = facebookProfile.Email,
                DisplayName = $"{facebookProfile.FirstName} {facebookProfile.LastName}.",
                PictureSource = facebookProfile.Picture.Data.Url,
            };
        }

        public static Profile FromEmail(string email, string displayName = null)
        {
            return new Profile()
            {
                Email = email,
                DisplayName = !string.IsNullOrEmpty(displayName) ? displayName : email.Split('@')[0],
            };
        }
    }
}
