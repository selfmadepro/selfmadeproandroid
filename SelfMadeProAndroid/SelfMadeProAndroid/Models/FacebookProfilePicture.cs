﻿using Newtonsoft.Json;

namespace SelfMadeProAndroid.Models
{
    public class FacebookProfilePicture
    {
        [JsonProperty("data")]
        public FacebookProfilePictureData Data { get; set; }
    }

    public class FacebookProfilePictureData
    {
        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("is_silhouette")]
        public bool IsSilhouette { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
