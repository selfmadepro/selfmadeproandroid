﻿using Newtonsoft.Json;
using SelfMadeProAndroid.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SelfMadeProAndroid.Models
{
    public class Chat
    {
        public Chat()
        {
            Id = Guid.NewGuid(); // создание идентификатора для нового чата
        }

        public Guid Id { get; set; }
        public List<BaseProfile> Profiles { get; set; } = new List<BaseProfile>();
        public List<ChatMessage> Messages { get; set; } = new List<ChatMessage>();
        
        [JsonIgnore]
        public string Title
        {
            get
            {
                var profiles = new List<string>();
                foreach (var p in Profiles)
                {
                    if (p.Uid != ProfileRepository.CurrentProfile.Uid)
                    {
                        profiles.Add(p.DisplayName);
                    }
                }

                if (profiles.Any())
                {
                    return string.Join(", ", profiles);
                }

                return ProfileRepository.CurrentProfile.DisplayName;
            }
        }
    }
}
