﻿namespace SelfMadeProAndroid.Models
{
    public class AuthResult
    {
        public string Token { get; set; }
        public string Uid { get; set; }
        public bool IsEmailVerified { get; set; }
    }
}
