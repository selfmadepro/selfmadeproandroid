﻿using Newtonsoft.Json;
using Plugin.FacebookClient;
using Plugin.FacebookClient.Abstractions;
using SelfMadeProAndroid.Models;
using SelfMadeProAndroid.Repository;
using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace SelfMadeProAndroid
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private async void FacebookButtonClicked(object sender, EventArgs e)
        {
            try
            {
                // вызов авторизации Facebook (повлечет за собой открытие окна или смены экрана для ввода логина/пароля Facebook)
                var fbLoginResponse = await CrossFacebookClient.Current.RequestUserDataAsync(
                    new string[] { "email", "first_name", "last_name", "picture" },
                    new string[] { "email" });

                if (fbLoginResponse.Status == FacebookActionStatus.Completed)
                {
                    // в случае успешной авторизации, получаем активный токен от фейсбук
                    var fbUserAccessToken = CrossFacebookClient.Current.ActiveToken;

                    // получаем объект сервиса для работы с Firebase на Android
                    var firebaseAuthenticator = DependencyService.Get<IFirebaseAuthenticator>();

                    // инициируем авторизацию на Firebase с помощью токена от Facebook и получаем Firebase токен в ответ
                    var authResult = await firebaseAuthenticator.SignInWithFacebookTokenAsync(fbUserAccessToken);

                    TokenRepository.StoreFirebaseToken(authResult.Token);

                    var profile = await ProfileRepository.GetProfileAsync(authResult.Uid);
                    if (profile == null)
                    {
                        // конвертируем результаты полученные от Facebook в вид модели
                        var facebookProfile = JsonConvert.DeserializeObject<FacebookProfile>(fbLoginResponse.Data);
                        profile = Profile.FromFacebookProfile(facebookProfile);
                        profile.Uid = authResult.Uid;

                        await ProfileRepository.SaveProfileAsync(profile);
                    }

                    // преобраование Facebook пользователя в профиль Firebase и сохранение в свойство
                    ProfileRepository.CurrentProfile = profile;

                    (Application.Current as App).MainPage = new NavigationPage(new MainPage());
                }
                else
                {
                    DisplayAlert("title", fbLoginResponse.Message, "OK");
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("title", ex.Message, "OK");
            }
        }

        private async void LoginViaEmail(object sender, EventArgs e)
        {
            // выполняем базовую валидацию
            if (string.IsNullOrEmpty(EmailEntry.Text) || string.IsNullOrEmpty(PasswordEntry.Text))
            {
                await DisplayAlert("Ошибка", "Пожалуйста заполните все поля!", "ОК");
                return;
            }

            // получаем объект сервиса для работы с Firebase на Android
            var firebase = DependencyService.Get<IFirebaseAuthenticator>();

            try
            {
                // делаем запрос на создание пользователя и получения токена
                var authResult = await firebase.SignInWithEmailAndPasswordAsync(EmailEntry.Text, PasswordEntry.Text);

                if (!authResult.IsEmailVerified)
                {
                    await firebase.SendEmailVerificationAsync();
                    await DisplayAlert("успешно", $"пользователь авторизирован и письмо для подтверждения отправлено", "OK");
                    return;
                }

                TokenRepository.StoreFirebaseToken(authResult.Token);

                var profile = await ProfileRepository.GetProfileAsync(authResult.Uid);
                if (profile == null)
                {
                    profile = Profile.FromEmail(EmailEntry.Text);
                    profile.Uid = authResult.Uid;
                    profile.IsEmailVerified = authResult.IsEmailVerified;

                    await ProfileRepository.SaveProfileAsync(profile);
                }
                
                // преобраование Facebook пользователя в профиль Firebase и сохранение в свойство
                ProfileRepository.CurrentProfile = profile;

                (Application.Current as App).MainPage = new NavigationPage(new MainPage());
            }
            catch (Exception ex)
            {
                await DisplayAlert("ошибка", ex.Message, "OK");
            }
        }

        private void RegisterWithEmail(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegisterWithEmailPage());
        }

        private async void ResetPasswordTapped(object sender, EventArgs e)
        {
            // выполняем проверку на ввод email
            if (string.IsNullOrEmpty(EmailEntry.Text))
            {
                await DisplayAlert("Ошибка", "Пожалуйста введите email", "ОК");
                return;
            }

            // получаем объект сервиса для работы с Firebase на Android
            var firebase = DependencyService.Get<IFirebaseAuthenticator>();

            try
            {
                // делаем запрос на отправку письма со сбросом пароля
                await firebase.SendPasswordResetEmailAsync(EmailEntry.Text);

                await DisplayAlert("отправлено", $"письмо с инструкциями отправлено на email '{EmailEntry.Text}'", "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("ошибка", ex.Message, "OK");
            }
        }
    }
}
