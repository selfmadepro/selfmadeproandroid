﻿using SelfMadeProAndroid.Models;
using SelfMadeProAndroid.Repository;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SelfMadeProAndroid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllProfilesPage : ContentPage
    {
        public AllProfilesPage()
        {
            InitializeComponent();
            BindingContext = this;
            RefreshListCommand.Execute(null);
        }

        public ObservableCollection<BaseProfile> Profiles { get; set; } = new ObservableCollection<BaseProfile>();
        public Command RefreshListCommand => new Command(async () => { await RefreshListAsync(); });
        public Command GoToChatCommand => new Command(GoToChat);

        private async void GoToChat(object obj)
        {
            var selectedProfile = obj as BaseProfile;
            var chat = await ChatRepository.GetOrCreateChatWithProfile(selectedProfile);
            await Navigation.PushAsync(new ChatPage(chat));
        }

        private void ProfileItemTapped(object sender, ItemTappedEventArgs e)
        {
            // выбранный элемент в списке обнуляется для того, чтобы убрать выделенный цвет
            ProfilesListView.SelectedItem = null;
        }

        private async Task RefreshListAsync()
        {
            Profiles.Clear();
            try
            {
                var profiles = await ProfileRepository.GetProfilesAsync();
                profiles.ForEach(Profiles.Add);
                OnPropertyChanged(nameof(Profiles));
            }
            catch (Exception e)
            {
                await DisplayAlert("ошибка", e.Message, "OK");
            }
            finally
            {
                ProfilesListView.IsRefreshing = false;
            }
        }
    }
}