﻿using SelfMadeProAndroid.Models;
using System.Threading.Tasks;

namespace SelfMadeProAndroid
{
    public interface IFirebaseAuthenticator
    {
        Task<AuthResult> SignInWithFacebookTokenAsync(string accessToken);
        Task<AuthResult> SignInWithEmailAndPasswordAsync(string email, string password);
        Task<AuthResult> CreateUserWithEmailAndPasswordAsync(string email, string password);
        Task SendPasswordResetEmailAsync(string email);
        Task SendEmailVerificationAsync();
        Task<bool> IsCurrentUserEmailVerified();
    }
}
