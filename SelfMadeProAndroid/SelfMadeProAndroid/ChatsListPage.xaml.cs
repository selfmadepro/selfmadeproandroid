﻿using SelfMadeProAndroid.Models;
using SelfMadeProAndroid.Repository;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SelfMadeProAndroid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatsListPage : ContentPage
    {
        public ChatsListPage()
        {
            InitializeComponent();
            BindingContext = this;
            RefreshListCommand.Execute(null);
        }

        public ObservableCollection<Chat> Chats { get; set; } = new ObservableCollection<Chat>();
        public Command RefreshListCommand => new Command(async () => { await RefreshListAsync(); });

        private async void ChatItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedChat = ChatsListView.SelectedItem as Chat;
            ChatsListView.SelectedItem = null;
            await Navigation.PushAsync(new ChatPage(selectedChat));
        }

        private async Task RefreshListAsync()
        {
            Chats.Clear();
            try
            {
                var profiles = await ChatRepository.GetCurrentProfileChatsAsync();
                profiles.ForEach(Chats.Add);
                OnPropertyChanged(nameof(Chats));
            }
            catch (Exception e)
            {
                await DisplayAlert("ошибка", e.Message, "OK");
            }
            finally
            {
                ChatsListView.IsRefreshing = false;
            }
        }
    }
}