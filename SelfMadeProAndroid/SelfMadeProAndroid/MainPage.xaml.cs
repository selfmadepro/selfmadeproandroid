﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SelfMadeProAndroid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}