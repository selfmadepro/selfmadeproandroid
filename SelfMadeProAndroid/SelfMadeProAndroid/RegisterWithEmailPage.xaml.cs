﻿using SelfMadeProAndroid.Models;
using SelfMadeProAndroid.Repository;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SelfMadeProAndroid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterWithEmailPage : ContentPage
    {
        public RegisterWithEmailPage()
        {
            InitializeComponent();
        }

        private async void Register(object sender, System.EventArgs e)
        {
            // выполняем базовую валидацию
            if (string.IsNullOrEmpty(EmailEntry.Text) || 
                string.IsNullOrEmpty(PasswordEntry.Text) || string.IsNullOrEmpty(Password2Entry.Text) ||
                PasswordEntry.Text != Password2Entry.Text)
            {
                await DisplayAlert("Ошибка", "Пожалуйста заполните все поля и удостоверьтесь что пароли совпадают!", "ОК");
                return;
            }

            // получаем объект сервиса для работы с Firebase на Android
            var firebase = DependencyService.Get<IFirebaseAuthenticator>();

            try
            {
                // делаем запрос на создание пользователя и получения токена
                var authResult = await firebase.CreateUserWithEmailAndPasswordAsync(EmailEntry.Text, PasswordEntry.Text);

                var profile = Profile.FromEmail(EmailEntry.Text, DisplayNameEntry.Text);
                profile.Uid = authResult.Uid;
                profile.IsEmailVerified = authResult.IsEmailVerified;

                TokenRepository.StoreFirebaseToken(authResult.Token);

                await ProfileRepository.SaveProfileAsync(profile);
                await DisplayAlert("успешно",$"Пользователь с адресом {EmailEntry.Text} успешно зарегистрирован","OK");
                // возвращаемся на предыдущую страницу в случае успешной регистрации
                await Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                await DisplayAlert("ошибка", ex.Message, "OK");
            }
        }
    }
}