﻿using SelfMadeProAndroid.Repository;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SelfMadeProAndroid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }

        // метод вызывается при каждом переходе на данную вкладку
        protected override void OnAppearing()
        {
            var profile = ProfileRepository.CurrentProfile;
            if (ProfileRepository.CurrentProfile == null)
            {
                DisplayAlert("ошибка", "текущий профиль не заполнен!", "ОК");
                return;
            }

            ProfileImageSource.Source = profile.ImageSource;
            ProfileEmailEntry.Text = profile.Email;
            ProfileDisplayNameEntry.Text = profile.DisplayName;
        }
    }
}